include:
  - project: 'chegginc/chegg-templates/gitlab-scripts/team-tags'
    ref: "master"
    file: '/martech.yml'
  - project: 'chegginc/chegg-templates/common'
    ref: "v1"
    file: '/templates/default-includes.yml'
  - project: 'chegginc/chegg-templates/gitlab-scripts/security-cicd'
    ref: "v1"
    file: '/chegg-sast.yml'

stages:
  - build
  - test
  - version
  - release
  - deploy

variables:
  # Hard code version now until we figure out the issue with major version tag for common images
  COMMON_VERSION: "1.4.1"
  NODE_YARN_IMAGE: ${CICD_ACCOUNT_NONPROD}.dkr.ecr.${CICD_REGION_NONPROD}.amazonaws.com/chegg-templates/common:node-yarn-python-${COMMON_VERSION}
  JAVA_IMAGE: ${CICD_ACCOUNT_NONPROD}.dkr.ecr.${CICD_REGION_NONPROD}.amazonaws.com/chegg-templates/common:java-kaniko-${COMMON_VERSION}
  AWS_CLI_IMAGE: ${CICD_ACCOUNT_NONPROD}.dkr.ecr.${CICD_REGION_NONPROD}.amazonaws.com/chegg-templates/common:docker-awscli-${COMMON_VERSION}

services:
  - docker:19.03-dind

image: ${JAVA_IMAGE}

build_jar:
  stage: build
  image: alpine:latest
  script:
    - echo "Building empty file"
    - mkdir build
    - echo "empty file created" > build/build.txt
  artifacts:
    paths:
      - build/

build_feature_job:
  stage: build
  extends:
    - .default_build_job
  rules:
    - if: $CI_MERGE_REQUEST_IID
  script:
    - ARTIFACTORY_PASSWORD=${ARTIFACTORY_PASSWORD_DEC} ./gradlew --no-daemon clean build -x test -x integrationTest

test_feature_job:
  stage: test
  extends:
    - .default_build_job
  rules:
    - if: $CI_MERGE_REQUEST_IID
  script:
    - ARTIFACTORY_PASSWORD=${ARTIFACTORY_PASSWORD_DEC} ./gradlew --no-daemon clean build test integrationTest

determine_version:
  stage: version
  image: alpine:latest
  extends:
    - .team_nonprod
  rules:
    - changes:
        - .cz.toml
      when: never
    - if: $CI_MERGE_REQUEST_IID
      when: never
    - when: always
  before_script:
    - echo "Determine Version Before Script Overwriten"
  script:
    - echo "Determine Version Script Overwritten"
    - mkdir version
    - touch version/version.txt
    - echo "hello" > version/version.txt
    - echo "got from the build jar step" > build/build.txt
    - cat build/build.txt
  artifacts:
    paths:
      - version/

push_release:
  stage: release
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  rules:
    - changes:
        - .cz.toml
      when: never
    - if: $CI_MERGE_REQUEST_IID
      when: never
    - when: always
  extends:
    - .default_build_job
  before_script:
    - echo "Determine Version Before Script Overwriten"
  script:
    - echo "Overwriting the default push release step"
    - aws configure list

push_dev:
  image: alpine:latest
  stage: deploy
  script:
    - cat ${BIN_PATH}/assume-role.sh
    - echo "push to the development stage"

.default_build_job:
  extends:
    - .build_cache
    - .team_nonprod
  before_script:
    - export ARTIFACTORY_PASSWORD_DEC=$(echo ${ARTIFACTORY_PASSWORD} | base64 -d)

.build_cache:
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - ./.gradle/caches/modules-2
    policy: pull-push
  rules:
    exists:
      - '**/*.groovy'
      - '**/*.java'
      - '**/*.scala'
